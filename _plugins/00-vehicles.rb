#
# Create a new page for each vehicle
#
#

require 'json'

module Jekyll

  class JsonDataPage < Jekyll::Page
    def initialize(site, base, dir, id, object)
    	@site = site
    	@base = base
    	@dir = dir
    	@name = id + ".json"
    	self.process(@name)
	self.read_yaml(File.join(base, '_layouts'), "json.json")
	self.data["object"] = object
    end
  end
  
  class YamlDataPage < Jekyll::Page
    def initialize(site, base, dir, id, object)
    	@site = site
    	@base = base
    	@dir = dir
    	@name = id + ".yaml"
    	self.process(@name)
	self.read_yaml(File.join(base, '_layouts'), "yaml.yaml")
	self.data["object"] = object
    end
  end
  


  class VehiclePage < Jekyll::Page
    def initialize(site, base, id, vehicle)
    	@site = site
    	@base = base
    	@dir = "vehicles"
    	@name = id + ".html"
    	self.process(@name)
	self.read_yaml(File.join(base, '_layouts'), "vehicle.html")
    	self.data["title"] = vehicle["name"] + " " + vehicle["my"].to_s
    	self.data["layout"] = "vehicle"
	self.data["vehicle"] = vehicle
	self.data["vehicle-id"] = id
    end
  end

  class VehicleGenerator < Jekyll::Generator


    def convert(vehicle, from, to, multiplier, digits)
    	if not vehicle[to] and vehicle[from]
    		vehicle[to] = (vehicle[from] * multiplier).round(digits)
    	end    
    end
   
    def kgf_to_n(vehicle, kgf, n)
    	convert(vehicle, kgf, n, 9.806650, 3)
    end

    def w_to_hp(vehicle, w, hp)
    	convert(vehicle, w, hp, 0.00134102, 3)
    end

    def hp_to_w(vehicle, hp, w)
    	convert(vehicle, hp, w, (1/0.00134102), 0)
    end

    def generate(site)
    	vehicles = site.data["vehicles"]
    	path = "_site/data"
    	FileUtils.mkpath(path) unless File.exist?(path)
	vehicleIds = []
	vehicleArray = []
        cycles = [ "epa", "wltp", "ndec", "pbev" ]	
	vehicles.each do |id,vehicle|
                	
		
		w_to_hp(vehicle, "ev-engine-power-w", "ev-engine-power")
		hp_to_w(vehicle, "ev-engine-power", "ev-engine-power-w")
		convert(vehicle, "ev-engine-power-w", "ev-engine-power-kw", 0.001, 2)
		
		kgf_to_n(vehicle, "ev-engine-torque-kgf", "ev-engine-torque")
		kgf_to_n(vehicle, "ice-engine-torque-kgf", "ice-engine-torque")
		kgf_to_n(vehicle, "combined-engine-torque-kgf", "combined-engine-torque")
		
		if vehicle["weight"] and vehicle["ev-engine-power"]
			vehicle["ev-weight-power"] = ( ( 1.0 * vehicle["weight"] ) / vehicle["ev-engine-power"] ).round(2)
		end
		
		if not vehicle["battery"] and vehicle["battery-packs"] and vehicle["battery-pack"]
			vehicle["battery"] = vehicle["battery-packs"] * vehicle["battery-pack"]
		end
		
		if not vehicle["battery"] and vehicle["battery-v"] and vehicle["battery-ah"]
			vehicle["battery"] = vehicle["battery-v"] * vehicle["battery-ah"]
		end

		# Se nao tem definicao de total utilizavel assume que eh total
		convert(vehicle, "battery", "battery-usable", 1, 0)
		
# https://insideevs.com/features/343231/heres-how-to-calculate-conflicting-ev-range-test-cycles-epa-wltp-nedc/
		convert(vehicle, "range-wltp", "range-epa", (1/1.12), 0)
		convert(vehicle, "range-ndec", "range-epa", (1/1.43), 0)
		convert(vehicle, "range-epa", "range-wltp", 1.12, 0)
		convert(vehicle, "range-epa", "range-ndec", 1.43, 0)
                if vehicle["pbev-consumo-energetico"] and vehicle["battery"]
		    vehicle["range-pbev"] = ( 3.6 * ((vehicle["battery-usable"] / 1000) / vehicle["pbev-consumo-energetico"])).round(0)
               	end
		convert(vehicle, "range-pbev", "range-pbev-descontado", 0.7, 0)
               	# EPA e PEBV deveriam ter os valores praticamente iguais
#		convert(vehicle, "range-pbev", "range-epa", 1, 0)
#		convert(vehicle, "range-epa", "range-pbev", 1, 0)
		
		if vehicle["battery-usable"] 

                        convert(vehicle, "battery-usable", "battery-usable-kwh", 1/1000.0, 1)
                        convert(vehicle, "battery", "battery-kwh", 1/1000.0, 1)
                        vehicle["battery-buffer"] = vehicle["battery"] - vehicle["battery-usable"]
                        
                        convert(vehicle, "battery-buffer", "battery-buffer-kwh", 1/1000.0, 1)

			if vehicle["range"]
    			    vehicle["consumption-kwh-100km"] = (( ( vehicle["battery-usable"] / 1000.0) / vehicle["range"] ) * 100).round(2)
			end
			
			if vehicle["range-epa"]
			    for cycle in cycles do
			        if vehicle["range-"+cycle]
    			            vehicle["consumption-kwh-100km-" + cycle] = (( ( vehicle["battery-usable"] / 1000.0) / vehicle["range-" + cycle] ) * 100).round(2)
			            # MJ/km
			            convert(vehicle, "consumption-kwh-100km-" + cycle,  "consumption-mj-km-" + cycle, (3.6 / 100), 2)

			            if site.config["cost-br-kwh"] 
				       vehicle["cost-br-100km-" + cycle] =  ( vehicle["consumption-kwh-100km-" + cycle] * site.config["cost-br-kwh"]).round(2)
				    end
			        end
			    end

			end	
		
		end
		for cycle in cycles do
		    if vehicle["price-br"] and vehicle["range-" + cycle]
			vehicle["price-br-range-"+cycle] = ( (1.0 * vehicle["price-br"]) / vehicle["range-"+cycle]).round(2)
		    end	
		end
		
		site.pages.push(VehiclePage.new(site, site.source, id, vehicle))

		site.pages.push(JsonDataPage.new(site, site.source, "vehicles", id, vehicle ))
		site.pages.push(YamlDataPage.new(site, site.source, "vehicles", id, vehicle ))

		vehicleIds.push(id)
		vehicleArray.push(vehicle)
		
	end	    	
	site.pages.push(JsonDataPage.new(site, site.source, "vehicles", "vehicles-ids", vehicleIds))
	site.pages.push(JsonDataPage.new(site, site.source, "vehicles", "vehicles-array", vehicleArray))
	site.pages.push(JsonDataPage.new(site, site.source, "vehicles", "vehicles", vehicles))
	site.pages.push(YamlDataPage.new(site, site.source, "vehicles", "vehicles", vehicles))
    end

  end
  
end

